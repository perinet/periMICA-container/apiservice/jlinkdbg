# API Service jlinkdbg

The apiservice security is the implementation of the security part from the
[unified api](https://gitlab.com/perinet/unified-api). The api service jlinkdbg is
available in all Perinets Products.

## References

* [openAPI spec for apiservice jlinkdbg] https://gitlab.com/perinet/unified-api/-/blob/main/services/Jlinkdbg_openapi.yaml


# Dual License

This software is by default licensed via the GNU Affero General Public License
version 3. However it is also available with a commercial license on request
(https://perinet.io/contact).