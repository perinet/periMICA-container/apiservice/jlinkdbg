module gitlab.com/perinet/periMICA-container/apiservice/jlinkdbg

go 1.18

require (
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20221122163058-59cb209fd3f2
	gitlab.com/perinet/generic/lib/utils v0.0.0-20221009100057-f2d2e5152487
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/exp v0.0.0-20221004215720-b9f4876ce741 // indirect
)
