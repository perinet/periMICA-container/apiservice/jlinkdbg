/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package jlinkdbg

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/shellhelper"
	"gitlab.com/perinet/generic/lib/utils/webhelper"
)

func PathsGet() []httpserver.Endpoint {
	return []httpserver.Endpoint{
		{Url: "/jlinkdbg", Method: httpserver.GET, Role: rbac.USER, Call: Jlinkdbg_Get},
		{Url: "/jlinkdbg/log", Method: httpserver.GET, Role: rbac.USER, Call: Jlinkdbg_Log_Get},
	}
}

const (
	API_VERSION     = "20"
	SYSTEMD_SERVICE = "jlinkgdbserver.service"
)

var (
	Logger log.Logger = *log.Default()
)

type Jlinkdbg struct {
	ApiVersion string `json:"api_version"`
}

func Jlinkdbg_Get(w http.ResponseWriter, r *http.Request) {
	binaryJson, err := json.Marshal(Jlinkdbg{ApiVersion: API_VERSION})
	if err != nil {
		webhelper.JsonResponse(w, http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	webhelper.JsonResponse(w, http.StatusOK, binaryJson)
}

func Jlinkdbg_Log_Get(w http.ResponseWriter, r *http.Request) {
	result := shellhelper.CommandCall("journalctl", "-u", SYSTEMD_SERVICE)

	webhelper.TextResponse(w, http.StatusOK, result.Stdout)
}
